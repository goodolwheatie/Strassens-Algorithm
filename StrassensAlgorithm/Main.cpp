#include <iostream>
#include "MatrixClass.h"

int main()
{
	MatrixClass test1(4);
	MatrixClass test2 = test1;
	MatrixClass test4;

	MatrixClass test3 = test1 + test2;

	std::cout << "TEST 1:\n";
	std::cout << test1 << "ADDRESS: " << &test1 << std::endl << std::endl;
	
	std::cout << "TEST 2:\n";
	std::cout << test2 << "ADDRESS: " << &test2 << std::endl << std::endl;
	
	std::cout << "TEST 3:\n";
	std::cout << test3 << "ADDRESS: " << &test3 << std::endl << std::endl;

	std::cout << std::endl << std::endl;
	system("pause");
	return 0;
}