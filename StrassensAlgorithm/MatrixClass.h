#pragma once
#ifndef MATRIXCLASS_H
#define MATRIXCLASS_H

#include <vector>
#include <iostream>

using matrix = std::vector<std::vector<int>>;

class MatrixClass
{
	
	friend std::ostream& operator<<(std::ostream& out, const MatrixClass&);
public:
	MatrixClass& operator=(const MatrixClass&);
	MatrixClass operator+(const MatrixClass&) const;
	MatrixClass operator-(const MatrixClass&) const;

	MatrixClass StrassensAlgorithm()
	{
		
	}

	MatrixClass(const MatrixClass&);
	~MatrixClass();

	MatrixClass();
	MatrixClass(int);
	void printMatrix() const;
	

private:
	matrix myMatrix;
	int dimension;

};






#endif 