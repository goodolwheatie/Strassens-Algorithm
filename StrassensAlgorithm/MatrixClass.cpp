#include "MatrixClass.h"
#include <random>
#include <iomanip>

std::ostream& operator<<(std::ostream& out, const MatrixClass& myMat)
{
	for (int i = 0; i < myMat.dimension; ++i)
	{
		for (const auto& element : myMat.myMatrix[i])
		{
			out << std::left << std::setw(3) << element << " ";
		}
		out << std::endl;
	}
	return out;
}

MatrixClass& MatrixClass::operator=(const MatrixClass& rhs)
{
	if (this != &rhs)
	{
		myMatrix = rhs.myMatrix;
		dimension = rhs.dimension;
	}
	return *this;
}

MatrixClass MatrixClass::operator+(const MatrixClass& rhs) const
{
	MatrixClass temp;
	temp.dimension = 0;

	if (dimension == rhs.dimension)
	{
		temp = *this;
		for (int i = 0; i < dimension; ++i)
		{
			for (int j = 0; j < dimension; ++j)
			{
				temp.myMatrix[i][j] += rhs.myMatrix[i][j];
			}
		}
	}
	else
	{
		std::cerr << "Cannot add matrices that have different dimensions.\n";
	}
	return temp;
}

MatrixClass MatrixClass::operator-(const MatrixClass& rhs) const
{
	MatrixClass temp;
	temp.dimension = 0;

	if (dimension == rhs.dimension)
	{
		temp = *this;
		for (int i = 0; i < dimension; ++i)
		{
			for (int j = 0; j < dimension; ++j)
			{
				temp.myMatrix[i][j] -= rhs.myMatrix[i][j];
			}
		}
	}
	else
	{
		std::cerr << "Cannot add matrices that have different dimensions.\n";
	}
	return temp;
}

MatrixClass::MatrixClass(const MatrixClass& rhs)
{
	myMatrix = rhs.myMatrix;
	dimension = rhs.dimension;
}

MatrixClass::~MatrixClass()
{
	
}

MatrixClass::MatrixClass()
{
	std::random_device rd;
	std::mt19937 mt(rd());
	std::uniform_int_distribution<> dist(0, 100);

	myMatrix.resize(2);
	for(int i = 0; i < 2; ++i)
	{
		for (int j = 0; j < 2; ++j)
		{
			myMatrix[i].emplace_back(dist(mt));
		}
	}
	dimension = 2;
}

MatrixClass::MatrixClass(int dim)
{
	std::random_device rd;
	std::mt19937 mt(rd());
	std::uniform_int_distribution<> dist(0, 100);

	myMatrix.resize(dim);
	for (int i = 0; i < dim; ++i)
	{
		for (int j = 0; j < dim; ++j)
		{
			myMatrix[i].emplace_back(dist(mt));
		}
	}
	dimension = dim;
}

void MatrixClass::printMatrix() const
{
	for (int i = 0; i < dimension; ++i)
	{
		for (const auto& element : myMatrix[i])
		{
			std::cout << std::left << std::setw(3) << element << " ";
		}
		std::cout << std::endl;
	}
}
